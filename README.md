# Volcano Docker

1. `docker compose build`

2. create a copy of file `volcano-template.yaml` with name `volcano.yaml` and configure it.

3. `docker compose up` (`-d` to run in background)
