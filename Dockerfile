FROM node:17-alpine3.14 as builder

WORKDIR /volcano

RUN apk upgrade -U --no-cache

RUN apk add curl --no-cache

# Download latest release file and extract needed files from it
RUN LOCATION=$(curl -s https://api.github.com/repos/AmandaDiscord/Volcano/releases/latest \
| grep "browser_download_url" \
| awk '{ print $2 }' \
| sed 's/,$//'       \
| sed 's/"//g' )     \
; curl -L -o data.zip $LOCATION && unzip data.zip "dist/*" -d . && unzip data.zip package.json && rm data.zip

# Node modules
RUN yarn --prod --frozen-lockfile

FROM node:17-alpine3.14 as app

WORKDIR /volcano

RUN apk upgrade -U --no-cache

RUN apk add ffmpeg --no-cache

# copy stuff needed to run
COPY --from=builder /volcano/node_modules /volcano/node_modules
COPY --from=builder /volcano/dist /volcano/dist
COPY --from=builder /volcano/package.json /volcano/

RUN chown -R node:node /volcano

EXPOSE 2333

USER node

CMD [ "node", "dist/index.js" ]